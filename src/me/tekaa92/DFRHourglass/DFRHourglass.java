package me.tekaa92.DFRHourglass;


import java.io.File;





import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class DFRHourglass extends JavaPlugin implements Listener {


	@Override
	public void onEnable() {
		//getServer().getLogger().setFilter(new CustomFilter()); //Gets the filter here
		getLogger().info("Plugin Enabled");
		getConfig().options().copyDefaults(true);
		saveConfig();
		dfrTimeCounter();


		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		scheduler.scheduleSyncDelayedTask(this, new Runnable() {
			@Override
			public void run() {
				//Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "time set 0");
				//Bukkit.getServer().broadcastMessage("time is now 06:00");
				Bukkit.getServer().getWorld(worldName).setTime(0);
				Bukkit.getServer().getWorld(worldName).setGameRuleValue("doDaylightCycle", "true");
			}
		}, 30L);  //A safeguard to make sure the server starts with a DaynightCycle
	}

	@Override
	public void onDisable() {

		getLogger().info("Plugin Disabled");

		//writeToFile();
	}

	//This will be connected to a config later
	int counterDays = 0;
	int counterYears = 0;
	int dayInterval = 0;
	int timeCounter = 0;
	int dayCounter = 0;
	int year = 0;
	//boolean dayCycleMidday = false;
	boolean dayTime = true;
	boolean gameruleCheckerDay = true;
	boolean gameruleCheckerNight = false;
	long secondsCounter = getConfig().getLong("Cycle_Time");
	int dayTestCounter = getConfig().getInt("dayTestCounter");
	int nightTestCounter = getConfig().getInt("nightTestCounter");
	String worldName = getConfig().getString("worldName");


	String[] loreDays = {"Lugus", "Sirona", "Bhall", "Nantosuelta", "Nemed","Amathaon","Junil", "Arawn","Oghma","Danalin","Dagda","Kilmorph","Sucellus","Tali","Camulos","Aeron","Ceridwen",
			"Mammon", "Esus","Mulcarn","Agares","Brigit","Cernunnos","Kylorin" };

	String[] loreYears = {"The years after the Fall of Erebus", "The years of Restoration", "The years of Demise"};
	//end


	public synchronized void dfrTimeCounter(){

		BukkitScheduler task = Bukkit.getServer().getScheduler();
		task.scheduleSyncRepeatingTask(this, new Runnable() {

			public synchronized void run() {

				if(gameruleCheckerDay == true) {
					if(Bukkit.getServer().getWorld(worldName).getTime() >= 5000 && Bukkit.getServer().getWorld(worldName).getTime() <= 7000) {
						loreTimeExecutableDay();
						gameruleCheckerDay = false;

					} 
				}
				else if (gameruleCheckerNight == true) {
					if (Bukkit.getServer().getWorld(worldName).getTime() >= 17000 && Bukkit.getServer().getWorld(worldName).getTime() <= 19000) {
						loreTimeExecutableNight();
						gameruleCheckerNight = false;
					}
				}
			} 
		}, 0, secondsCounter);
	}

	/*
	//Method to turn off DaylightCycle
	private void loreTimeExecutablefalse() {
		Bukkit.getServer().getWorld("Somnium").setGameRuleValue("doDaylightCycle", "false");
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle false");		
	}*/

	//Method for DaytimeCycle
	private synchronized void loreTimeExecutableDay() {
		Bukkit.getServer().getWorld(worldName).setGameRuleValue("doDaylightCycle", "false");
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle false");
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {

			public void run() {
				Bukkit.getServer().getWorld(worldName).setGameRuleValue("doDaylightCycle", "true");
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle true");
				gameruleCheckerNight = true;
				//task.notify();
			}
		}, dayTestCounter);
	}

	//Method for NighttimeCycle
	private synchronized void loreTimeExecutableNight() {
		Bukkit.getServer().getWorld(worldName).setGameRuleValue("doDaylightCycle", "false");
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle false");
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {

			public void run() {
				Bukkit.getServer().getWorld(worldName).setGameRuleValue("doDaylightCycle", "true");
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle true");
				gameruleCheckerDay = true;

			}
		}, nightTestCounter);
	}


	//Here is the command interface
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		if (sender instanceof Player) {

			Player player = (Player) sender;
			if(commandLabel.equalsIgnoreCase("LoreDay")) {
				player.sendMessage(ChatColor.AQUA + "The day is: " + loreDays[dayInterval]);

			}
		}
		return false;
	}


	public void writeToFile() throws Exception {

		File file = new File(this.getDataFolder() + "config.yml");
		FileConfiguration fc = YamlConfiguration.loadConfiguration(file);

		fc.isSet("# a basic config for DFRHourglass"
				+ "20ticks is 1 second");
		fc.set("Cycle_Time: ", secondsCounter);
		fc.save(file);
		getLogger().info("[DFRHourglass] saved");
	}
}

